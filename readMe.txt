Pagina de Front-end da empresa j�nior Humaniza Consultoria J�nior em Gest�o de Pessoas possuindo elementos como barra de
navega��o (navbar), elementos informativos da empresa e seus servi�os.

As tecnologias usadas para desenvolver esse projeto foram: Html, Css, Bootstrap, Javascript.

Conhecimentos adquiridos: Durante o desenvolvimento obtive o conhecimento em como usar varias fun��es do bootstrap para
que meu layout seja responsivo com base no que � fornecido na documenta��o do site oficial, conhecimento em associar 
funcionalidades ja existentes casados com outras obtendo um uso dinamico de varias funcionalidades que se auto complementam.
Algumas delas descritas s�o slideshow/carousel para manipula��o de imagens, container e container-fluid, conceitos de colunas
e arrows, formularios e outros elementos proporcionando maior facilidade e produtividade na cria��o deste Front-end.

Desafios que tive: Tive dificuldades em compreender como fazer o bootstrap � inserido no meu projeto html, em como funciona
a responsividade e tamb�m que nem sempre estou livre de usar um css proprio porque algumas situa��o pedem que corrija 
pontos que o bootstrap peca e n�o existe solu��o existente dentro da documenta��o oficial.